﻿namespace Assets.Scripts
{
    public class AddListenerToDaysCountSingleton : AddToIntUnityEventListener
    {
        public override void AddListener()
        {
            if (DaysValue.SINGLETON) DaysValue.SINGLETON.OnValueUpdated.AddListener(CallListeners);
        }
    }
}
