﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Helpers;
using MattRGeorge.Unity.Utilities.Enums;

namespace Assets.Scripts
{
    public class IntVariable : MonoBehaviour
    {
        [SerializeField] protected int intValue = 0;
        public UnityStartMethods setOn = UnityStartMethods.Awake;
        public IntUnityEvent OnValueUpdated = new IntUnityEvent();

        public virtual int IntValue
        {
            get => intValue;
            set
            {
                intValue = value;
                OnValueUpdated?.Invoke(intValue);
            }
        }

        public virtual void AddBy(int amount)
        {
            IntValue += amount;
        }
        public virtual void MultiplyBy(int amount)
        {
            IntValue *= amount;
        }
        public virtual void DivideBy(int amount)
        {
            IntValue /= amount;
        }

        protected virtual void Awake()
        {
            if (setOn == UnityStartMethods.Awake) IntValue = intValue;
        }
        protected virtual void Start()
        {
            if (setOn == UnityStartMethods.Start) IntValue = intValue;
        }
    }
}
