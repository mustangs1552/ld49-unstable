﻿namespace Assets.Scripts.Interfaces
{
    /// <summary>
    /// Is a deliver destination for mail.
    /// </summary>
    public interface IIsDeliveryDestination
    {
        bool CanBeDeliveredTo();
        bool AttemptDelivery();
    }
}
