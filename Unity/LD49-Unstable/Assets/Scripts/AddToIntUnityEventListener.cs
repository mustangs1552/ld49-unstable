﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Helpers;
using MattRGeorge.Unity.Utilities.Enums;

namespace Assets.Scripts
{
    public abstract class AddToIntUnityEventListener : MonoBehaviour
    {
        public UnityStartMethods addOn = UnityStartMethods.None;
        public IntUnityEvent AddedListener = new IntUnityEvent();

        public abstract void AddListener();

        public virtual void CallListeners(int value)
        {
            AddedListener?.Invoke(value);
        }

        protected virtual void Awake()
        {
            if (addOn == UnityStartMethods.Awake) AddListener();
        }
        protected virtual void Start()
        {
            if (addOn == UnityStartMethods.Start) AddListener();
        }
    }
}
