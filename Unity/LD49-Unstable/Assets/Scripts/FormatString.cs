﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Utilities.Helpers;
using MattRGeorge.Unity.Utilities.Enums;

namespace Assets.Scripts
{
    public class FormatString : MonoBehaviour
    {
        [TextArea(0, 50)]
        [SerializeField] protected string textTemplate = "";
        [SerializeField] protected string paramOne = "";
        [SerializeField] protected string paramTwo = "";
        [SerializeField] protected string paramThree = "";
        public UnityStartMethods updateOn = UnityStartMethods.Start;
        public StringUnityEvent OnFormattedStringUpdated = new StringUnityEvent();

        public virtual string TextTemplate
        {
            get => textTemplate;
            set
            {
                textTemplate = (value == null) ? "" : value;
                UpdateFormattedString();
            }
        }
        public virtual string ParamOne
        {
            get => paramOne;
            set
            {
                paramOne = (value == null) ? "" : value;
                UpdateFormattedString();
            }
        }
        public virtual string ParamTwo
        {
            get => paramTwo;
            set
            {
                paramTwo = (value == null) ? "" : value;
                UpdateFormattedString();
            }
        }
        public virtual string ParamThree
        {
            get => paramThree;
            set
            {
                paramThree = (value == null) ? "" : value;
                UpdateFormattedString();
            }
        }

        public virtual string FormatedString => string.Format(textTemplate, paramOne, paramTwo, paramThree);

        protected string formattedString = "";

        protected virtual void UpdateFormattedString()
        {
            formattedString = FormatedString;
            OnFormattedStringUpdated?.Invoke(formattedString);
        }

        protected virtual void OnValidate()
        {
            TextTemplate = textTemplate;
            ParamOne = paramOne;
            ParamTwo = paramTwo;
            ParamThree = paramThree;
        }

        protected virtual void Awake()
        {
            if(updateOn == UnityStartMethods.Awake) UpdateFormattedString();
        }
        protected virtual void Start()
        {
            if (updateOn == UnityStartMethods.Start) UpdateFormattedString();
        }
    }
}
