﻿using UnityEngine;
using UnityEngine.Events;
using MattRGeorge.Unity.Utilities.Helpers;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts
{
    /// <summary>
    /// A basic delivery destination for mail.
    /// </summary>
    public class DeliveryDestination : MonoBehaviour, IIsDeliveryDestination
    {
        [Tooltip("Is this destination available for delivery?")]
        [SerializeField] protected bool isAvailable = true;
        public UnityEvent OnDeliveryMade = new UnityEvent();
        public BoolUnityEvent OnAvailabilityUpdated = new BoolUnityEvent();

        /// <summary>
        /// Is this destination available for delivery?
        /// </summary>
        public virtual bool IsAvailable
        {
            get => isAvailable;
            set
            {
                bool oldValue = isAvailable;
                isAvailable = value;
                if (oldValue != isAvailable) OnAvailabilityUpdated?.Invoke(isAvailable);
            }
        }

        /// <summary>
        /// Can this destination currently take delivery?
        /// </summary>
        /// <returns>True is can.</returns>
        public virtual bool CanBeDeliveredTo()
        {
            return isAvailable;
        }

        /// <summary>
        /// Attempt to make a delivery.
        /// </summary>
        /// <returns>True if successful.</returns>
        public virtual bool AttemptDelivery()
        {
            if(CanBeDeliveredTo())
            {
                IsAvailable = false;
                OnDeliveryMade?.Invoke();
                return true;
            }

            return false;
        }
    }
}
