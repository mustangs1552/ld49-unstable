﻿using UnityEngine;

namespace Assets.Scripts
{
    /// <summary>
    /// A simple hazard object.
    /// </summary>
    public class HazardObject : MonoBehaviour
    {
        [Tooltip("The slime damage that this object deals (percentage).")]
        [Range(0, 100)]
        [SerializeField] protected float damage = 10;

        /// <summary>
        /// The slime damage that this object deals.
        /// </summary>
        public virtual float Damage
        {
            get => damage;
            set
            {
                if (value < 0) damage = 0;
                else if (value > 100) damage = 100;
                else damage = value;
            }
        }
    }
}
