﻿using UnityEngine;
using Assets.Scripts.Interfaces;
using MattRGeorge.Unity.Utilities.Helpers;

namespace Assets.Scripts
{
    /// <summary>
    /// Manages the players mail count.
    /// </summary>
    public class PlayerMail : MonoBehaviour
    {
        [Tooltip("Starting amount of mail.")]
        [SerializeField] protected int mailCount = 0;
        public IntUnityEvent OnMailCountUpdated = new IntUnityEvent();

        /// <summary>
        /// Amount of mail.
        /// </summary>
        public virtual int MailCount
        {
            get => mailCount;
            set
            {
                mailCount = (value < 0) ? 0 : value;
                OnMailCountUpdated?.Invoke(mailCount);
            }
        }

        /// <summary>
        /// Attempts to deliver mail to the given object.
        /// </summary>
        /// <param name="obj">The object to check (must have a component that implements `IIsDeliveryDestination`).</param>
        /// <returns>True if successful.</returns>
        public virtual bool AttemptDelivery(GameObject obj)
        {
            if (!obj) return false;

            return AttemptDelivery(obj.GetComponent<IIsDeliveryDestination>());
        }
        /// <summary>
        /// Intended to be used for collisions.
        /// Attempts to deliver mail to the given collision's gameobject.
        /// </summary>
        /// <param name="obj">Not used.</param>
        /// <param name="collision">The collision involved. `collision.gameobject` must have a component that implements `IIsDeliveryDestination`.</param>
        /// <returns>True if successful.</returns>
        public virtual void AttemptDelivery(GameObject obj, Collision collision)
        {
            if (collision == null) return;

            AttemptDelivery(collision.gameObject.GetComponent<IIsDeliveryDestination>());
        }
        /// <summary>
        /// Attempts to deliver mail to the given object.
        /// </summary>
        /// <param name="deliveryDestination">The object to deliver mail to.</param>
        /// <returns>True if successful.</returns>
        public virtual bool AttemptDelivery(IIsDeliveryDestination deliveryDestination)
        {
            if (deliveryDestination == null || mailCount == 0) return false;

            if (deliveryDestination.AttemptDelivery())
            {
                MailCount--;
                return true;
            }

            return false;
        }

        protected virtual void OnValidate()
        {
            MailCount = mailCount;
        }

        protected virtual void Awake()
        {
            MailCount = mailCount;
        }
    }
}
