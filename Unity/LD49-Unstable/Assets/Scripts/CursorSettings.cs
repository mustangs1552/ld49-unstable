﻿using UnityEngine;

namespace Assets.Scripts
{
    public class CursorSettings : MonoBehaviour
    {
        public virtual CursorLockMode LockState
        {
            get => Cursor.lockState;
            set => Cursor.lockState = value;
        }

        public virtual bool Visible
        {
            get => Cursor.visible;
            set => Cursor.visible = value;
        }

        public virtual void SetLockStateNone()
        {
            Cursor.lockState = CursorLockMode.None;
        }
        public virtual void SetLockStateLocked()
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        public virtual void SetLockStateConfined()
        {
            Cursor.lockState = CursorLockMode.Confined;
        }
    }
}
