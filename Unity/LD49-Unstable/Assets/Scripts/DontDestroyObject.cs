﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Enums;

namespace Assets.Scripts
{
    public class DontDestroyObject : MonoBehaviour
    {
        public UnityStartMethods setOn = UnityStartMethods.Awake;

        public virtual void SetDontDestroyOnLoad()
        {
            DontDestroyOnLoad(gameObject);
        }

        protected virtual void Awake()
        {
            if (setOn == UnityStartMethods.Awake) DontDestroyOnLoad(gameObject);
        }
        protected virtual void Start()
        {
            if (setOn == UnityStartMethods.Start) DontDestroyOnLoad(gameObject);
        }
    }
}
