﻿using UnityEngine;
using MattRGeorge.Unity.Tools.Stats;

namespace Assets.Scripts
{
    /// <summary>
    /// The player's slime supply and maangement of taking slime and shrinking.
    /// </summary>
    public class PlayerSlimeSupply : MonoBehaviour
    {
        [Tooltip("Player's supply of slime and limits.")]
        [SerializeField] protected StatGauge slimeSupply = new StatGauge(100, 100, false);
        [Tooltip("The piece of slime that is spawned and scaled when touching a hazard.")]
        public GameObject slimePiece = null;
        [Tooltip("The player to scale down as slime is taken away.")]
        [SerializeField] protected Transform playerBlobMesh = null;

        /// <summary>
        /// Player's supply of slime.
        /// </summary>
        public virtual float SlimeSupply
        {
            get => slimeSupply.CurrAmount;
            set => slimeSupply.CurrAmount = value;
        }

        /// <summary>
        /// The player's blob mesh to scale as slime is lost.
        /// </summary>
        public virtual Transform PlayerBlobMesh
        {
            get => playerBlobMesh;
            set
            {
                playerBlobMesh = value;
                if (playerBlobMesh) playerStartScale = playerBlobMesh.localScale;
            }
        }

        protected Vector3 playerStartScale = Vector3.zero;

        /// <summary>
        /// Attempt to take slime damage from the given collision.
        /// </summary>
        /// <param name="obj">Not used.</param>
        /// <param name="collision">The collision with the object to check and point to spawn the slime piece.</param>
        public virtual void AttemptTakeDamage(GameObject obj, Collision collision)
        {
            if (collision == null) return;

            HazardObject hazard = collision.gameObject.GetComponent<HazardObject>();
            if (hazard)
            {
                float startSlime = slimeSupply.CurrAmount;
                slimeSupply.CurrAmount -= hazard.Damage;
                if (startSlime != slimeSupply.CurrAmount)
                {
                    ScalePlayer(hazard.Damage);
                    SpawnSlimePiece(hazard.Damage, collision);
                }
            }
        }

        /// <summary>
        /// Scale the player down as they loose slime by the slime damage taken.
        /// </summary>
        /// <param name="damageTaken">The slime damage taken.</param>
        protected virtual void ScalePlayer(float damageTaken)
        {
            if (!playerBlobMesh || damageTaken <= 0) return;

            Vector3 newScale = Vector3.zero;
            newScale.x = (damageTaken / 100) * playerStartScale.x;
            newScale.y = (damageTaken / 100) * playerStartScale.y;
            newScale.z = (damageTaken / 100) * playerStartScale.z;
            playerBlobMesh.localScale -= newScale;
            playerBlobMesh.localScale = new Vector3(
                (playerBlobMesh.localScale.x < 0) ? 0 : playerBlobMesh.localScale.x,
                (playerBlobMesh.localScale.y < 0) ? 0 : playerBlobMesh.localScale.y,
                (playerBlobMesh.localScale.z < 0) ? 0 : playerBlobMesh.localScale.z);
        }

        /// <summary>
        /// Spawn the slime piee and place at the collision point and scale to match slime damage taken.
        /// </summary>
        /// <param name="damageTaken">The slime damage taken.</param>
        /// <param name="collision">The collision with the contact point and object to latch to.</param>
        protected virtual void SpawnSlimePiece(float damageTaken, Collision collision)
        {
            if (!slimePiece || damageTaken <= 0 || collision == null) return;

            GameObject piece = Instantiate(slimePiece, collision.GetContact(0).point, Quaternion.identity, collision.gameObject.transform);
            piece.transform.localScale *= damageTaken / 100;
        }

        protected virtual void Awake()
        {
            slimeSupply.StatReset();

            PlayerBlobMesh = playerBlobMesh;
        }
    }
}
