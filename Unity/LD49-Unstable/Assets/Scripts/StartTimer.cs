﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Components;

namespace Assets.Scripts
{
    public class StartTimer : MonoBehaviour
    {
        public Timer timer = null;

        public void StartTheTimer()
        {
            if (timer) timer.StartCountdown();
        }
    }
}
