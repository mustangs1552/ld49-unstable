﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Helpers;

namespace Assets.Scripts
{
    public class ToString : MonoBehaviour
    {
        public StringUnityEvent Result = new StringUnityEvent();

        public virtual void Convert(int value)
        {
            Result?.Invoke(value.ToString());
        }
    }
}
