﻿using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts
{
    /// <summary>
    /// The base loose condition object that all other loose conditions inherit from.
    /// </summary>
    public class LooseCondition : MonoBehaviour
    {
        public UnityEvent OnLostInitiated = new UnityEvent();

        /// <summary>
        /// Inititite the loose condition including calling the event.
        /// </summary>
        public virtual void InitiateLost()
        {
            OnLostInitiated?.Invoke();
        }
    }
}
