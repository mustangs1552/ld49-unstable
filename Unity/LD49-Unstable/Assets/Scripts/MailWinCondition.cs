﻿namespace Assets.Scripts
{
    /// <summary>
    /// Win condition for when all mail is delivered.
    /// </summary>
    public class MailWinCondition : WinCondition
    {
        /// <summary>
        /// Check that the given mail count meets the win condition requirements.
        /// </summary>
        /// <param name="mailCount">The mail count to check.</param>
        public virtual void CheckMailCount(int mailCount)
        {
            if (mailCount <= 0)
            {
                InitiateWin();
                if (DaysValue.SINGLETON) DaysValue.SINGLETON.IntValue++;
            }
        }
    }
}
