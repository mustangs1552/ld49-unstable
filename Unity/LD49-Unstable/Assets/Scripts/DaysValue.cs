﻿using UnityEngine;

namespace Assets.Scripts
{
    public class DaysValue : IntVariable
    {
        public static DaysValue SINGLETON = null;

        protected override void Awake()
        {
            if (SINGLETON)
            {
                Debug.LogError($"There is already a '{GetType()}' singleton!");
                Destroy(this);
            }
            else SINGLETON = this;

            base.Awake();
        }
    }
}
