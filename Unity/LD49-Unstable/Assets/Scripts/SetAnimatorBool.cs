﻿using UnityEngine;

namespace Assets.Scripts
{
    public class SetAnimatorBool : MonoBehaviour
    {
        public Animator animator = null;
        public string boolName = "";

        public void SetBool(bool value)
        {
            if (!animator) animator = GetComponent<Animator>();
            if (!animator || string.IsNullOrEmpty(boolName)) return;

            animator.SetBool(boolName, value);
        }
    }
}
