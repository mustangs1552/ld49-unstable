﻿using UnityEngine;

namespace Assets.Scripts
{
    public class QuitGame : MonoBehaviour
    {
        public virtual void Quit()
        {
            Application.Quit();
        }
    }
}
