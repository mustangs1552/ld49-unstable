﻿using UnityEngine;

namespace Assets.Scripts
{
    public class UIGuage : MonoBehaviour
    {
        public RectTransform fill = null;
        [SerializeField] protected Vector3 fillStartScale = new Vector3(1, 1, 1);
        [SerializeField] protected RectTransform.Axis scaleAxis = RectTransform.Axis.Horizontal;
        [Range(0, 1)]
        [SerializeField] protected float amountFilled = 1;
        [SerializeField] protected bool scaleByScale = false;

        public virtual float AmountFilled
        {
            get => amountFilled;
            set
            {
                if (!fill || value < 0 || value > 1) return;

                if (!scaleByScale) fill.SetSizeWithCurrentAnchors(scaleAxis, fillStartSize * value);
                else
                {
                    if (scaleAxis == RectTransform.Axis.Horizontal) fill.localScale = new Vector3(value * fillStartSize, fill.localScale.y);
                    if (scaleAxis == RectTransform.Axis.Vertical) fill.localScale = new Vector3(fill.localScale.x, value * fillStartSize);
                }
                amountFilled = value;
            }
        }

        protected float fillStartSize = 0;

        public virtual void UpdateFillStartSize()
        {
            if (!fill) return;

            fill.localScale = fillStartScale;
            if (!scaleByScale)
            {
                if (scaleAxis == RectTransform.Axis.Horizontal) fillStartSize = fill.sizeDelta.x;
                else if (scaleAxis == RectTransform.Axis.Vertical) fillStartSize = fill.sizeDelta.y;
            }
            else
            {
                if (scaleAxis == RectTransform.Axis.Horizontal) fillStartSize = fill.localScale.x;
                else if (scaleAxis == RectTransform.Axis.Vertical) fillStartSize = fill.localScale.y;
            }
        }

        protected virtual void Awake()
        {
            UpdateFillStartSize();
            AmountFilled = amountFilled;
        }
    }
}
