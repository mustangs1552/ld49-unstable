﻿using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts
{
    /// <summary>
    /// The base win condition object that all other win conditions inherit from.
    /// </summary>
    public class WinCondition : MonoBehaviour
    {
        public UnityEvent OnWinInitiated = new UnityEvent();

        /// <summary>
        /// Inititite the win condition including calling the event.
        /// </summary>
        public virtual void InitiateWin()
        {
            OnWinInitiated?.Invoke();
        }
    }
}
