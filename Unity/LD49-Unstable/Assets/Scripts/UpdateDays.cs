﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Helpers;

namespace Assets.Scripts
{
    public class UpdateDays : MonoBehaviour
    {
        public IntUnityEvent OnUpdate = new IntUnityEvent();

        protected virtual void FixedUpdate()
        {
            if (DaysValue.SINGLETON) OnUpdate?.Invoke(DaysValue.SINGLETON.IntValue);
        }
    }
}
