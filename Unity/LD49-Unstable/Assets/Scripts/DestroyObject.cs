﻿using UnityEngine;

namespace Assets.Scripts
{
    public class DestroyObject : MonoBehaviour
    {
        public string objectName = "";
        public bool destroyImmediate = false;

        public virtual void Destroy()
        {
            if (!string.IsNullOrEmpty(objectName))
            {
                if (destroyImmediate) DestroyImmediate(GameObject.Find(objectName));
                else Destroy(GameObject.Find(objectName));
            }
        }
    }
}
